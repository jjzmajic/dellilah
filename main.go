package main

import (
	"github.com/go-chat-bot/bot/slack"
	_ "github.com/go-chat-bot/plugins/catfacts"
	_ "github.com/go-chat-bot/plugins/catgif"
	_ "github.com/go-chat-bot/plugins/chucknorris"
	_ "gitlab.com/jjzmajic/dellilah/logger"
	_ "gitlab.com/jjzmajic/dellilah/scheduling"
	// Import all the commands you wish to use
)

func main() {
	slack.Run(SlackToken)
}
